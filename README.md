# README #

**ATTENTION** Ce projet a été réalisé avec Unity 2017.3, et *doit rester sur cette version* tant que Unity 2018 n'est pas plus stable.

## Organisation des dossiers

Le dossier **StibTableProject** contient le projet Unity dans son entièreté. les fichiers en dehors de ce dossier sont des fichiers temporaires pouvant être supprimés.

###Assets :
Le dossier **Assets** est subdivisé en 4 sous-dossiers, et ne contient aucun fichiers :
- **Custom Assets** contient les fichier créé par les personnes ayant travailler directement sur le projet
- ** Imported Assets** contient les assets importés du Unity Asset Store, ou trouvés en ligne
- **Standard Assets** contient les assets standard de Unity
- **Plugins** contient des utilitaires

###Scripts :
Le dossier **Scripts** est sous-divisé en 3 dossiers :
- **API** contenant uniquement les scripts directement liés à l'API de la STIB
- **Core** contenant les scripts clefs pour la gestions des trams dans la scene
- **Secondary** contenant les scripts liés à :
	- la gestion de la caméra
	- les effets visuels
	- les décorations


## TO DO ###

###1 
* **Tâche** : Refaire les scripts du tram manager et des trams.
* **Pourquoi** : Le tram manager doit gérer en interne toutes les distances, de manière à éviter les bugs visuels tels que 2 trams passant l'un à travers l'autre, ou des trams se téléportant.
* **Problème** : refonte complète des scripts
* **Solutions possibles** :
a) avoir un seul Tram Manager pour l'ensemble de la scène
b) avoir un Line Manager, gérant ses tram en interne

###2
* **Tâche** : Implémenter le téléchargement des fichier GTS de la STIB, 1 fois par semaine
* **Pourquoi** : les DirectionsID et LineID sont actuellement hardcodé, ce qui rend l'application obsolète dès qu'il y a des traveaux sur la ligne.
* **Problèmes** : 
	- le fichier .txt à lire se trouve dans un fichier ZIP, qu'il faut dézipper dans Unity,
	- lors du téléchargement, le fichier n'est pas télécharger correctement, et l'archive devient inutilisable.

###3
* **Tâche** : implémenter une génération de fausses données
* **Pourquoi** : avoir une démo fonctionelle même lorsque il n'y a pas de connection internet disponible, on que la Stib n'envoie pas les données nécessaire
* **Problèmes** :
	- comment générer ces donnéess?
	- comment décider s'il faut utiliser ces données, où celles de la STIB?

###4
* **Tâche** :
    - refaire/modifier le modèle 3D de la ville
    - faire des texture pour la ville
    - séparer les matérieux de certains bâtiments, de manière à pouvoir les mettre en couleurs 
* **Pourquoi** :
    - actuellement, la vue arérienne est fort moche, et consomme beaucoup de ressources pour aficher le modèle 3D car il n'est pas du tout optimisé,
    - il serait utile de pouvoir mettre les batiments de la STIB et de l'IRISIB en surbrillance.
* **Problèmes** :
	- refaire un modèle 3D, ce n'est pas simple
    - refaire des texture, ça prend du temps
    - il n'est possible de vérifier quon a fait du bon travail que après l'avoir finit, ce qui rend la tâche chronophage
	
###5
* **Tâche** : créer un indicateur discret, mais visible que la récupération/le traitement des données n'a pas fonctionner.
* **Pourquoi** : actuellement, seule la console dans Unity permet de savoir qu'il y a un problème, or le build doit pouvoir lui aussi indiquer s'il y a un problème (tel que problème de connectioninternet).